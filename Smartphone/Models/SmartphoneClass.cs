﻿
namespace Smartphone.Models
{
   /// <summary>
   /// our sweet smartphone class
   /// </summary>
   public class SmartphoneClass
   {
    /// <summary>
    /// Smartphone Weight
    /// </summary>
    public int Weight { get; set; }
    
    /// <summary>
    /// Smartphone Water Resistance.
    /// </summary>
    public bool WaterResistance { get; set; }
    
    ///summary
    /// Smartphone battery property.
    /// </summary>
    public Battery Battery { get; set; }
    
    /// <summary>
    /// Smartphone Apps property.
    /// </summary>
    public Apps Apps { get; set; }
   
    /// <summary>
    /// Smartphone Radio Features property.
    /// </summary>
    public RadioFeatures RadioFeatures { get; set; }
   
    /// <summary>
    /// Smartphone OS property.
    /// </summary>
    public OS OS { get; set; }
   
    /// <summary>
    /// Smartphone Rear Camera property.
    /// </summary>
    public Camera RearCamera { get; set; }
   
    /// <summary>
    /// Smartphone Front Camera property.
    /// </summary>
    public Camera FrontCamera { get; set; }
  
    /// <summary>
    /// Smartphone CPU class.
    /// </summary>
    public CPU CPU { get; set; }
 
    /// <summary>
    /// Smartphone chassis property.
    /// </summary>
    public Chassis Chassis { get; set; }
  
    /// <summary>
    /// Smartphone Ram property.
    /// </summary>
    public Ram Ram { get; set; }

    /// <summary>
    /// Smartphone Screen property.
    /// </summary>
    public Screen Screen { get; set; }

    /// <summary>
    /// The make of the smartphone.
    /// </summary>
    public string Make { get; set; }

    /// <summary>
    /// The model of the smartphone.
    /// </summary>
    public string Model { get; set; }

    /// <summary>
    /// Phone's diagonal screen size in inches.
    /// </summary>
    public decimal ScreenSizeIn { get; set; }

    /// <summary>
    /// Read-only screen size in mm.
    /// </summary>
    public decimal ScreenSizeMm
    {
      get
      {
        return ScreenSizeIn * (decimal)25.4;
      }
    }
       
    /// <summary>
    /// Screen width in pixels.
    /// </summary>
    public int ScreenWidth { get; set; }

    /// <summary>
    /// Screen height in pixels.
    /// </summary>
    public int ScreenHeight { get; set; }

    /// <summary>
    /// Front camera megapixels.
    /// </summary>
    public decimal FrontCameraMegapixels { get; set; }

    /// <summary>
    /// Rear camera megapixels.
    /// </summary>
    public decimal RearCameraMegapixels { get; set; }
  }
}
