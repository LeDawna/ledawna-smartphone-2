﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smartphone.Utility;

namespace Smartphone.Models
{
  public class Chassis
  {
    public List<Lookups.MaterialTypes> MaterialTypes { get; set; }
    public decimal Length { get; set; }
    public decimal Width { get; set; }
    public decimal Height { get; set; }
  }
}
