﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smartphone.Utility;

namespace Smartphone.Models
{
  public class Screen
  {
    public Lookups.ScreenTypes ScreenType { get; set; }
    public bool IsTouchScreen { get; set; }
    public decimal Size { get; set; }
    public decimal ResWidth { get; set; }
    public decimal ResHeight { get; set; }
    public int PPI { get; set; }
  }
}
