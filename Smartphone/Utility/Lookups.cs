﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartphone.Utility
{
  public static class Lookups
  {
    public enum ScreenTypes
    {
      //cast to int to get #, etc.
      Amoled = 1,
      Lcd = 2,
      Led = 3,
    }
    public enum MaterialTypes
    {
      Aluminum = 1,
      CarbonFiber = 2,
      Glass = 3,
      Plastic = 4
     }
 }
}
