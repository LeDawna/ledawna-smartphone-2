﻿using Smartphone.Models;
using Smartphone.Utility;
using System;
using System.Collections.Generic;

namespace Smartphone
{
  class Program
  {
    static void Main(string[] args)
    {
     
      var sc = new SmartphoneClass();

      sc.Make = "Apple";
      sc.Model = "iPhone 6 Plus";
      sc.Weight = 5;
      sc.WaterResistance = false;
      sc.Ram = new Ram ();
      sc.Ram.Size = (decimal)1120;

      sc.Screen = new Screen();
      sc.Screen.ScreenType = Lookups.ScreenTypes.Led;
      sc.Screen.IsTouchScreen = true;
      sc.Screen.Size = (decimal) 5.5;
      sc.Screen.ResWidth = (decimal) 2.2;
      sc.Screen.ResHeight = (decimal) 3.2;
      sc.Screen.PPI = 401;

      sc.Chassis = new Chassis ();
      sc.Chassis.MaterialTypes = new List<Lookups.MaterialTypes>();
      sc.Chassis.MaterialTypes.Add(Lookups.MaterialTypes.Aluminum);
      sc.Chassis.MaterialTypes.Add(Lookups.MaterialTypes.CarbonFiber);
      sc.Chassis.MaterialTypes.Add(Lookups.MaterialTypes.Glass);
      sc.Chassis.MaterialTypes.Add(Lookups.MaterialTypes.Plastic);
      sc.Chassis.Length = (decimal)5.95;
      sc.Chassis.Width = (decimal)3.95;
      sc.Chassis.Height = (decimal)5.15;

      sc.CPU = new CPU ();
      sc.CPU.Make =  "TSMC";
      sc.CPU.Model = "A8";
      sc.CPU.ClockSpeed = (decimal) 2.6;

      sc.FrontCamera = new Camera();
      sc.FrontCamera.HasFlash = true;
      sc.FrontCamera.ResWidth = 1080;
      sc.FrontCamera.ResHeight = 1920;

      sc.RearCamera = new Camera();
      sc.RearCamera.HasFlash = true;
      sc.RearCamera.ResWidth = 680;
      sc.RearCamera.ResHeight = 1020;

      sc.OS = new OS();
      sc.OS.Name = "iOS";
      sc.OS.Version = "8";

      sc.RadioFeatures = new RadioFeatures();
      sc.RadioFeatures.HasGPS = true;
      sc.RadioFeatures.HasWifi = true;
      sc.RadioFeatures.HasLte = true;
      sc.RadioFeatures.HasCdma = false;

      sc.Apps = new Apps();
      sc.Apps.App1 = "Instagram";
      sc.Apps.App2 = "Beats";
      sc.Apps.App3 = "Cartwheel";

      sc.Battery = new Battery();
      sc.Battery.mAh = 2500;
      sc.Battery.IsRemovable = false;
      sc.Battery.MaxTalkTime = 1440;

      //Write Output//

      Console.WriteLine("***** LeDawna's Amazing Smartphone ******");
      
      Console.WriteLine();
      Console.WriteLine("Make? {0}", sc.Make);
      Console.WriteLine("Model? {0}", sc.Model);

      Console.WriteLine();
      Console.WriteLine("Weight? {0}", sc.Weight);
      Console.WriteLine("Water Resistant? {0}", sc.WaterResistance ? "Yes" : "No");
      Console.WriteLine("Ram? {0}", sc.Ram.Size);
      
      Console.WriteLine();
      Console.WriteLine("Touchscreen? {0}", sc.Screen.IsTouchScreen ? "Yes":"No");
      Console.WriteLine("Screen Type? {0}", sc.Screen.ScreenType);
      Console.WriteLine("Screen Size? {0} in.", sc.Screen.Size);
      Console.WriteLine("Screen Resolution? {0} in. x {1} in.", sc.Screen.ResWidth, sc.Screen.ResHeight);
      Console.WriteLine("Screen PPI? {0}", sc.Screen.PPI);
      
      Console.WriteLine();
      foreach (var material in sc.Chassis.MaterialTypes)
	      {
		       Console.WriteLine("Chassis Material Types: {0}",material);
	      }
      Console.WriteLine("Chassis Dimensions: Length? {0}, Width? {1}, Height? {2}", sc.Chassis.Length, sc.Chassis.Width, sc.Chassis.Height);
      
      Console.WriteLine();
      Console.WriteLine("CPU Make? {0}", sc.CPU.Make);
      Console.WriteLine("CPU Model? {0}", sc.CPU.Model);
      Console.WriteLine("CPU Clockspeed? {0}", sc.CPU.ClockSpeed);
     
      Console.WriteLine();
      Console.WriteLine("OS Name? {0}", sc.OS.Name);
      Console.WriteLine("OS Version? {0}", sc.OS.Version);
     
      Console.WriteLine();
      Console.WriteLine("Batery mAh? {0}", sc.Battery.mAh);
      Console.WriteLine("Removable? {0}", sc.Battery.IsRemovable);
      Console.WriteLine("Max Talk Time? {0}", sc.Battery.MaxTalkTime);
     
     Console.WriteLine();
     if (sc.FrontCamera !=null)
	    {
     Console.WriteLine("Does the Front Camera have a flash? {0}",sc.FrontCamera.HasFlash ? "Yes" : "No");
     Console.WriteLine("Front Camera Resolution Width? {0}", sc.FrontCamera.ResWidth);
		 Console.WriteLine("Front Camera Resolution Height? {0}",sc.FrontCamera.ResHeight);
	    }
      else
      {
        Console.WriteLine("No Front Camera");
      }
      
     Console.WriteLine();
      if (sc.RearCamera != null)
      {
        Console.WriteLine("Does the Rear Camera have a flash? {0}", sc.RearCamera.HasFlash ? "Yes" : "No");
        Console.WriteLine("Rear Camera Resolution Width? {0}", sc.RearCamera.ResWidth);
        Console.WriteLine("Rear Camera Resolution Height? {0}", sc.RearCamera.ResHeight);
      }
      else
      {
        Console.WriteLine("This phone does not have a rear camera");
      }
      
      Console.WriteLine();
      Console.WriteLine("Favorite apps? {0}, {1}, and {2}.", sc.Apps.App1, sc.Apps.App2, sc.Apps.App3);

      Console.WriteLine();
      if (sc.RadioFeatures != null)
      {
        Console.WriteLine("Does the radio have GPS? {0}", sc.RadioFeatures.HasGPS ? "Yes" : "No");
        Console.WriteLine("Does the radio have Wifi? {0}", sc.RadioFeatures.HasWifi ? "Yes" : "No");
        Console.WriteLine("Does the radio use Lte {0}", sc.RadioFeatures.HasLte ? "Yes" : "No");
        Console.WriteLine("Does the radio use Cdma? {0}", sc.RadioFeatures.HasCdma ? "Yes" : "No");
      }
      else
      {
        Console.WriteLine("This Smartphone does not have a radio...it's not really a phone then is it?");
      }
  
      Console.ReadKey();
    }
  }
}
